(function ($) {
    "use strict";
    
    $(".hero-carousel").owlCarousel({
        center: true,
        margin: 30,
        autoplay: false,
        dots: false,
        loop: true,
        nav: true,
        items: 1,
	    /*navText: ['<span class="fas fa-chevron-left fa-2x"></span>','<span class="fas fa-chevron-right fa-2x"></span>'],
        */responsive: { 0: { items: 2 }, 768: { items: 4 }, 900: { items: 3 }
        }
    });
        // Testimonials carousel (uses the Owl Carousel library)
    $(".testimonials-carousel").owlCarousel({
        autoplay: true,
        dots: true,
        nav: true,
        loop: true,
        items: 1
    });
    
    $(".clients-carousel").owlCarousel({
        autoplay: true,
        dots: true,
        loop: true,
        responsive: { 0: { items: 2 }, 768: { items: 4 }, 900: { items: 5 }
        }
    });
})(jQuery);