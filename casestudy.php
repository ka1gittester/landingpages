<?php
/*
 * Template Name: Case Study
 * description: >-
  Page template without sidebar
 */
 ?>

<html lang="en-US" class="js"><head>
        <meta charset="UTF-8">
    <meta name="description" content=""><meta name="description" content="The Open University and Uber partnership"><meta name="keywords" content=""><meta name="keywords" content="Uber, The open University, free learning, degrees, qualifications"><meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="pingback" href="https://skillshub.online/xmlrpc.php">
    
        <script async="" src="//www.google-analytics.com/analytics.js"></script><script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
            document.documentElement.className = 'js';
        </script>
    
        <script>var et_site_url='https://skillshub.online';var et_post_id='50379';function et_core_page_resource_fallback(a,b){"undefined"===typeof b&&(b=a.sheet.cssRules&&0===a.sheet.cssRules.length);b&&(a.onerror=null,a.onload=null,a.href?a.href=et_site_url+"/?et_core_page_resource="+a.id+et_post_id:a.src&&(a.src=et_site_url+"/?et_core_page_resource="+a.id+et_post_id))}
    </script><title>skillshub.online – skillshub.online</title>
    <link rel="dns-prefetch" href="//www.google.com">
    <link rel="dns-prefetch" href="//fonts.googleapis.com">
    <link rel="dns-prefetch" href="//s.w.org">
    <link rel="alternate" type="application/rss+xml" title="skillshub.online » Feed" href="https://skillshub.online/feed/">
    <link rel="alternate" type="application/rss+xml" title="skillshub.online » Comments Feed" href="https://skillshub.online/comments/feed/">
    <!-- This site uses the Google Analytics by MonsterInsights plugin v7.10.1 - Using Analytics tracking - https://www.monsterinsights.com/ -->
    <script type="text/javascript" data-cfasync="false">
        var mi_version         = '7.10.1';
        var mi_track_user      = true;
        var mi_no_track_reason = '';
        
        var disableStr = 'ga-disable-UA-149267967-1';
    
        /* Function to detect opted out users */
        function __gaTrackerIsOptedOut() {
            return document.cookie.indexOf(disableStr + '=true') > -1;
        }
    
        /* Disable tracking if the opt-out cookie exists. */
        if ( __gaTrackerIsOptedOut() ) {
            window[disableStr] = true;
        }
    
        /* Opt-out function */
        function __gaTrackerOptout() {
          document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
          window[disableStr] = true;
        }
        
        if ( mi_track_user ) {
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');
    
            __gaTracker('create', 'UA-149267967-1', 'auto');
            __gaTracker('set', 'forceSSL', true);
            __gaTracker('require', 'displayfeatures');
            __gaTracker('send','pageview');
        } else {
            console.log( "" );
            (function() {
                /* https://developers.google.com/analytics/devguides/collection/analyticsjs/ */
                var noopfn = function() {
                    return null;
                };
                var noopnullfn = function() {
                    return null;
                };
                var Tracker = function() {
                    return null;
                };
                var p = Tracker.prototype;
                p.get = noopfn;
                p.set = noopfn;
                p.send = noopfn;
                var __gaTracker = function() {
                    var len = arguments.length;
                    if ( len === 0 ) {
                        return;
                    }
                    var f = arguments[len-1];
                    if ( typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function' ) {
                        console.log( 'Not running function __gaTracker(' + arguments[0] + " ....) because you are not being tracked. " + mi_no_track_reason );
                        return;
                    }
                    try {
                        f.hitCallback();
                    } catch (ex) {
    
                    }
                };
                __gaTracker.create = function() {
                    return new Tracker();
                };
                __gaTracker.getByName = noopnullfn;
                __gaTracker.getAll = function() {
                    return [];
                };
                __gaTracker.remove = noopfn;
                window['__gaTracker'] = __gaTracker;
                        })();
            }
    </script>
   <script src="https://skillshub.online/wp-includes/js/wp-emoji-release.min.js?ver=5.3" type="text/javascript" defer=""></script><script src="https://skillshub.online/wp-includes/js/wp-emoji-release.min.js?ver=5.3" type="text/javascript" defer=""></script>
            <meta content="Uber and OU v.1.0" name="generator"><style type="text/css">
    img.wp-smiley,
    img.emoji {
        display: inline !important;
        border: none !important;
        box-shadow: none !important;
        height: 1em !important;
        width: 1em !important;
        margin: 0 .07em !important;
        vertical-align: -0.1em !important;
        background: none !important;
        padding: 0 !important;
    }
    </style>
        <link rel="stylesheet" id="wp-block-library-css" href="https://skillshub.online/wp-includes/css/dist/block-library/style.min.css?ver=5.3" type="text/css" media="all">
    <link rel="stylesheet" id="contact-form-7-css" href="https://skillshub.online/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.6" type="text/css" media="all">
    <link rel="stylesheet" id="cookie-law-info-css" href="https://skillshub.online/wp-content/plugins/cookie-law-info/public/css/cookie-law-info-public.css?ver=1.8.2" type="text/css" media="all">
    <link rel="stylesheet" id="cookie-law-info-gdpr-css" href="https://skillshub.online/wp-content/plugins/cookie-law-info/public/css/cookie-law-info-gdpr.css?ver=1.8.2" type="text/css" media="all">
    <link rel="stylesheet" id="et_monarch-css-css" href="https://skillshub.online/wp-content/plugins/monarch/css/style.css?ver=1.4.12" type="text/css" media="all">
    <link rel="stylesheet" id="et-gf-open-sans-css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" type="text/css" media="all">
    <link rel="stylesheet" id="parent-style-css" href="https://skillshub.online/wp-content/themes/Divi/style.css?ver=5.3" type="text/css" media="all">
    <link rel="stylesheet" id="divi-style-css" href="https://skillshub.online/wp-content/themes/Your-Generated-Divi-child-theme-template-by-DiviCake/style.css?ver=4.0.6" type="text/css" media="all">
    <link rel="stylesheet" id="cf7-styler-styles-css" href="https://skillshub.online/wp-content/plugins/cf7-styler-for-divi/styles/style.min.css?ver=1.0.0" type="text/css" media="all">
    <link rel="stylesheet" id="et-builder-googlefonts-cached-css" href="https://fonts.googleapis.com/css?family=Montserrat%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2Cregular%2Citalic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&amp;ver=5.3#038;subset=latin,latin-ext" type="text/css" media="all">
    <link rel="stylesheet" id="ds-pbe-fb-css" href="https://skillshub.online/wp-content/plugins/ds-page-builder-everywhere/pbe-li.css?ver=5.3" type="text/css" media="all">
    <link rel="stylesheet" id="dashicons-css" href="https://skillshub.online/wp-includes/css/dashicons.min.css?ver=5.3" type="text/css" media="all">
    <link rel="stylesheet" id="cf7cf-style-css" href="https://skillshub.online/wp-content/plugins/cf7-conditional-fields/style.css?ver=1.7.8" type="text/css" media="all">
    <link rel="stylesheet" id="divi-ultimate-header-plugin-main-css-css" href="https://skillshub.online/wp-content/plugins/Divi-Ultimate-Header-Plugin/css/main.css?ver=3.0.0.9" type="text/css" media="all">
    <script type="text/javascript">
    /* <![CDATA[ */
    var monsterinsights_frontend = {"js_events_tracking":"true","download_extensions":"doc,pdf,ppt,zip,xls,docx,pptx,xlsx","inbound_paths":"[{\"path\":\"\\\/go\\\/\",\"label\":\"affiliate\"},{\"path\":\"\\\/recommend\\\/\",\"label\":\"affiliate\"}]","home_url":"https:\/\/skillshub.online","hash_tracking":"false"};
    /* ]]> */
    </script>
    <script type="text/javascript" src="https://skillshub.online/wp-content/plugins/google-analytics-for-wordpress/assets/js/frontend.min.js?ver=7.10.1"></script>
    <script type="text/javascript" src="https://skillshub.online/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp"></script>
    <script type="text/javascript" src="https://skillshub.online/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1"></script>
    <script type="text/javascript">
    /* <![CDATA[ */
    var Cli_Data = {"nn_cookie_ids":[],"cookielist":[]};
    var log_object = {"ajax_url":"https:\/\/skillshub.online\/wp-admin\/admin-ajax.php"};
    /* ]]> */
    </script>
    <script type="text/javascript" src="https://skillshub.online/wp-content/plugins/cookie-law-info/public/js/cookie-law-info-public.js?ver=1.8.2"></script>
    <script type="text/javascript" src="https://skillshub.online/wp-content/themes/Your-Generated-Divi-child-theme-template-by-DiviCake/custom.js?ver=5.3"></script>
    <link rel="https://api.w.org/" href="https://skillshub.online/wp-json/">
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://skillshub.online/xmlrpc.php?rsd">
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://skillshub.online/wp-includes/wlwmanifest.xml"> 
    <meta name="generator" content="WordPress 5.3">
    <link rel="canonical" href="https://skillshub.online/">
    <link rel="shortlink" href="https://skillshub.online/">
    <link rel="alternate" type="application/json+oembed" href="https://skillshub.online/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fskillshub.online%2F">
    <link rel="alternate" type="text/xml+oembed" href="https://skillshub.online/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fskillshub.online%2F&amp;format=xml">
    <style>.nav li.et-hover > ul,
    .menu li.et-hover > ul {
        visibility: visible !important;
        opacity: 1 !important; }
    .da11y-submenu-show {
        visibility: visible !important;
    }
    .keyboard-outline {
            outline: #5ceff9 solid 2px;
        -webkit-transition: none !important;
        transition: none !important;
    }
    button:active.keyboard-outline,
    button:focus.keyboard-outline,
    input:active.keyboard-outline,
    input:focus.keyboard-outline,
    a[role="tab"].keyboard-outline {
        outline-offset: -5px;
    }
    .et-search-form input:focus.keyboard-outline {
        padding-left: 15px;
        padding-right: 15px;
    }
    .et_pb_tab {
        -webkit-animation: none !important;
        animation: none !important;
    }
    .et_pb_contact_form_label,
    .widget_search .screen-reader-text,
    .et_pb_search .screen-reader-text {
        display: block !important; }
    .da11y-screen-reader-text,
    .et_pb_contact_form_label,
    .widget_search .screen-reader-text,
    .et_pb_search .screen-reader-text {
        clip: rect(1px, 1px, 1px, 1px);
        position: absolute !important;
        height: 1px;
        width: 1px;
        overflow: hidden;
        text-shadow: none;
        text-transform: none;
        letter-spacing: normal;
        line-height: normal;
        font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif;
        font-size: 1em;
        font-weight: 600;
        -webkit-font-smoothing: subpixel-antialiased;
    }
    .da11y-screen-reader-text:focus {
        background: #f1f1f1;
        color: #00547A;
        -webkit-box-shadow: 0 0 2px 2px rgba(0,0,0,.6);
        box-shadow: 0 0 2px 2px rgba(0,0,0,.6);
        clip: auto !important;
        display: block;
        height: auto;
        left: 5px;
        padding: 15px 23px 14px;
        text-decoration: none;
        top: 7px;
        width: auto;
        z-index: 1000000; }
    </style><meta name="viewport" content="width=device-width, initial-scale=1.0">	
        <style type="text/css"> 		body.free-vertical-navigation-body-tag .free-hamburger-icon .mobile_menu_bar:before { color: #e51b23; }
            .free-vertical-navigation-background-overlay { background-color: rgba(255,255,255,0.85); }
            .free-vertical-navigation, .free-vertical-navigation-wrapper { max-width: 805px!important; width: 100%!important; }
            .free-vertical-navigation .fullwidth-menu-nav>ul>li>ul.sub-menu { left: 805px; }
            .free-vertical-navigation .fullwidth-menu-nav>ul>li.et-reverse-direction-nav>ul.sub-menu { left: auto; right: 805px; }
            
            .free-vertical-navigation-menu-hover-1 .free-vertical-navigation nav>ul>li>a:before,
                .free-vertical-navigation-menu-hover-2 .free-vertical-navigation nav>ul>li>a:before,
                    .free-vertical-navigation-menu-hover-3 .free-vertical-navigation nav>ul>li:hover>a,
                        .free-vertical-navigation-menu-hover-4 .free-vertical-navigation nav>ul>li>a:before {
                background-color: #e51b23!important;
            }
            body.free-vertical-navigation-custom-menu-hover .free-vertical-navigation nav>ul>li:hover>a {
                color: #ffffff!important;
            }
            
            @media screen and (min-width: 981px) {
                html.et-fb-root-ancestor:not(.et-fb-preview--wireframe) .free-vertical-navigation {
                    margin-left: -805px!important;
                }
                html.et-fb-root-ancestor:not(.et-fb-preview--wireframe) body.free-vertical-navigation-body-tag.et-db.et-bfb>article #page-container-bfb .et-fb-post-content,
                    html.et-fb-root-ancestor:not(.et-fb-preview--wireframe) body.free-vertical-navigation-body-tag .et-fb-post-content {
                    margin-left: 805px!important;
                }
                html.et-fb-root-ancestor:not(.et-fb-preview--wireframe) body.free-vertical-navigation-body-tag.et-db.et-bfb>article #page-container-bfb .et-fb-post-content .free-vertical-navigation {
                    position: relative!important;
                    float: left !important;
                }
            }
            
                    
                @media screen and (min-width: 1140px) {
                    html.et-fb-root-ancestor:not(.et-fb-preview--wireframe) .free-vertical-navigation-breakpoint-show {
                        opacity: 0.5!important;
                    }
                    html:not(.et-fb-root-ancestor) .free-vertical-navigation {
                        position: fixed!important;
                        left: 0;
                    }
                    html:not(.et-fb-root-ancestor) .free-vertical-navigation-wrapper .free-vertical-navigation {
                        position: relative!important;
                    }
                    html:not(.et-fb-root-ancestor):not(.et-fb-preview--wireframe) body.free-vertical-navigation-body-tag #et-main-area,
                        body.free-vertical-navigation-body-tag .free-du-plugin-header {
                        margin-left: 805px!important;
                    }
                    html:not(.et-fb-root-ancestor) .free-vertical-navigation-breakpoint-show {
                        display: none!important;
                    }
                    .free-vertical-navigation-background-overlay {
                        display: none!important;
                    }
                    .free-vertical-navigation-wrapper:not(.free-menu-collapsible-wrapper) {
                        box-shadow: none!important;
                    }
                }
                @media screen and (max-width: 1139px) {
                    html.et-fb-root-ancestor:not(.et-fb-preview--wireframe) .free-vertical-navigation-breakpoint-hide,
                        html.et-fb-root-ancestor:not(.et-fb-preview--wireframe) .free-vertical-navigation {
                        opacity: 0.5!important;
                    }
                    html:not(.et-fb-root-ancestor) .free-vertical-navigation {
                        transform: translateX(-100%);
                    }
                    html:not(.et-fb-root-ancestor) .free-vertical-navigation-wrapper .free-vertical-navigation {
                        transform: translateX(0%);
                    }
                    body:not(.free-vertical-navigation-overlay-show) .free-vertical-navigation-wrapper {
                        box-shadow: none!important;
                    }
                }
                
                    
                @media screen and (max-width: 1139px) {
            
                            html:not(.et-fb-root-ancestor) .free-vertical-navigation-breakpoint-hide {
                        display: none!important;
                    }
                    body.free-vertical-navigation-overlay-left .free-vertical-navigation-wrapper {
                        transform: translateX(-100%);
                        transition: all 0.5s ease;
                    }
                    body.free-vertical-navigation-overlay-right .free-vertical-navigation-wrapper {
                        transform: translateX(100%);
                        left: auto;
                        right: 0;
                        transition: all 0.5s ease;
                    }
            
                    
                }
                
                                    .free-menu-hover-2 .free-header-menu nav>ul>li:before { background-color: #068293!important; }
                                                .free-menu-hover-2 .free-header-menu nav>ul>li:hover>a,
                                .free-menu-hover-2.et-db #et-boc .free-header-menu nav>ul>li:hover>a { color: #ffffff!important; opacity: 1!important; }
                            </style>     <style>
    
    
        </style>
    
    <style type="text/css">
    
    /** PBE CSS **/
    .page-template-page-template-blank.pbe-above-header #pbe-above-header-wa-wrap,
    .page-template-page-template-blank.pbe-below-header #pbe-below-header-wa-wrap,
    .page-template-page-template-blank.pbe-footer #pbe-footer-wa-wrap {
        display:none !important;
    }
    #pbe-above-content-wa-wrap .et_pb_widget {
        display: block;
        width: 100%;
        position: relative;
        /*margin-top: -15px;*/
        margin-bottom: 50px;
    }
    
    #pbe-above-content-wa-wrap .et_pb_section {
        z-index: 99;
    }
    
    #pbe-below-content-wa-wrap .et_pb_widget {
        display: block;
        width: 100%;
        position: relative;
        /*margin-top: -15px;*/
    }
    
    #pbe-below-content-wa-wrap .et_pb_section {
        z-index: 99;
    }
    
    #main-header .et_pb_widget, #content-area .et_pb_widget {
        width: 100%;
    }
    
    #main-header .et_pb_widget p {
        padding-bottom: 0;
    }
    
    #pbe-above-header-wa .widget-conditional-inner {
        background: #fff;
        padding: 0;
        border: none;
    }
    
    #pbe-above-header-wa select {
        background: #f1f1f1;
        box-shadow: none;
        border-radius: 3px;
        height: 40px;
        padding-left: 10px;
        padding-right: 10px;
        border: none;
    }
    
    #pbe-above-header-wa .et_pb_widget
    {
        float:none;
    }
    
    #pbe-footer-wa-wrap .et_pb_widget {
        width: 100%;
        display: block;
    }
    
    .page-container form input[type=text] {
        display: block;
        margin-bottom: 20px;
        width: 100%;
        background: #f1f1f1;
        padding: 10px 20px;
        box-shadow: none;
        border: none;
        font-weight: 700;
    }
    
    .page-container form p {
        font-size: 14px;
    }
    
    .page-container form {
        padding: 10px 20px;
    }
    
    #pbe-footer-wa-wrap {
        position: relative;
        /*top: -15px;*/
    }
    
    #pbe-above-header-wa-wrap,
    #pbe-below-header-wa-wrap,
    #pbe-above-content-wa-wrap,
    #pbe-below-content-wa-wrap,
    #pbe-footer-wa-wrap {
        position: relative;
        z-index: 9;
    }
    /* Fixes issues with overlapping widget areas */
    .pbe-above-header #main-header .container,
    .pbe-below-content #main-content article,
    .pbe-footer #main-footer { 
         clear: both;
     }
     .pbe-below-content #main-content {
        float: left;
        display: block;
        width: 100%;
    }
    .pbe-below-content #main-footer {
        float: left;
        width: 100%;
    }
    </style>
        <style type="text/css" id="et-social-custom-css">
                    
                </style>		<script type="text/javascript">
                var cli_flush_cache=2;
            </script>
            <style type="text/css" id="custom-background-css">
    body.custom-background { background-color: #ffffff; }
    </style>
        <script>
    jQuery(function($){
        $('.et_pb_accordion .et_pb_toggle_open').addClass('et_pb_toggle_close').removeClass('et_pb_toggle_open');
        $('.et_pb_accordion .et_pb_toggle').click(function() {     
          $this = $(this);
          setTimeout(function(){
          $this.closest('.et_pb_accordion').removeClass('et_pb_accordion_toggling');
    },700);
        });
    });
    </script>
    
    <script>
    jQuery(function($){
     $('.et_pb_toggle_title').click(function(){
      var $toggle = $(this).closest('.et_pb_toggle');
      if (!$toggle.hasClass('et_pb_accordion_toggling')) {
       var $accordion = $toggle.closest('.et_pb_accordion');
       if ($toggle.hasClass('et_pb_toggle_open')) {
        $accordion.addClass('et_pb_accordion_toggling');
        $toggle.find('.et_pb_toggle_content').slideToggle(700, function() { 
         $toggle.removeClass('et_pb_toggle_open').addClass('et_pb_toggle_close');
        });
       }
       setTimeout(function(){ 
        $accordion.removeClass('et_pb_accordion_toggling'); 
       }, 750);
      }
     });
    });
    </script><link rel="icon" href="https://skillshub.online/wp-content/uploads/2019/09/cropped-UberOU_Site-Identity-32x32.png" sizes="32x32">
    <link rel="icon" href="https://skillshub.online/wp-content/uploads/2019/09/cropped-UberOU_Site-Identity-192x192.png" sizes="192x192">
    <link rel="apple-touch-icon-precomposed" href="https://skillshub.online/wp-content/uploads/2019/09/cropped-UberOU_Site-Identity-180x180.png">
    <meta name="msapplication-TileImage" content="https://skillshub.online/wp-content/uploads/2019/09/cropped-UberOU_Site-Identity-270x270.png">
    <link rel="stylesheet" id="et-core-unified-50379-cached-inline-styles" href="https://skillshub.online/wp-content/et-cache/50379/et-core-unified-50379-15752988242389.min.css" onerror="et_core_page_resource_fallback(this, true)" onload="et_core_page_resource_fallback(this)"><style>[data-columns]::before{visibility:hidden;position:absolute;font-size:1px;}</style><style id="fit-vids-style">.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style><style>[data-columns]::before{visibility:hidden;position:absolute;font-size:1px;}</style></head>
    <body class="home page-template-default page page-id-50379 custom-background free-menu-hover-2 free-mega-menu free-vertical-navigation-overlay-right et_monarch et_pb_button_helper_class et_fullwidth_nav et_fixed_nav et_show_nav et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_header_style_left et_pb_footer_columns4 et_cover_background et_pb_gutter windows et_pb_gutters3 et_pb_pagebuilder_layout et_no_sidebar et_divi_theme et-db et_minified_js et_minified_css chrome et_mobile_device et_mobile_device_not_ipad"><a href="#main-content" class="skip-link da11y-screen-reader-text">Skip to content</a><a href="#main-content" class="skip-link da11y-screen-reader-text">Skip to content</a>
        <div id="page-container" class="et-animated-content" style="padding-top: 94px; margin-top: 9px;">
    
        
        
                <header id="main-header" data-height-onload="94" data-height-loaded="true" data-fixed-height-onload="104" class="et-fixed-header" style="top: 0px;"><div id="pbe-above-header-wa-wrap">
                </div>
                <div class="container clearfix et_menu_container">
                                <div class="logo_container">
                        <span class="logo_helper"></span>
                        <a href="https://skillshub.online/">
                            <img src="https://skillshub.online/wp-content/uploads/2019/09/OU_Uber_NewLogo_footer.png" alt="skillshub.online" id="logo" data-height-percentage="70" data-actual-width="322" data-actual-height="95">
                        </a>
                    </div>
                                <div id="et-top-navigation" data-height="80" data-fixed-height="90" style="padding-left: 277px;">
                                                <nav id="top-menu-nav">
                            <ul id="top-menu" class="nav"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-50379 current_page_item menu-item-50492"><a href="https://skillshub.online/" aria-current="page">OU and Uber Partnership</a></li>
    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-51792"><a href="https://skillshub.online/check-eligibility-launch/">Check your eligibility</a></li>
    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-50490"><a href="https://skillshub.online/study-at-the-open-university/">Study at The OU</a></li>
    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-51023"><a href="https://skillshub.online/ou-free-courses-on-openlearn/">OpenLearn</a></li>
    </ul>						</nav>
                        
                        
                        
                        
                        <div id="et_mobile_nav_menu">
                    <div class="mobile_nav closed">
                        <span class="select_page">Select Page</span>
                        <span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
                    <ul id="mobile_menu" class="et_mobile_menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-50379 current_page_item menu-item-50492 et_first_mobile_item"><a href="https://skillshub.online/" aria-current="page">OU and Uber Partnership</a></li>
    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-51792"><a href="https://skillshub.online/check-eligibility-launch/">Check your eligibility</a></li>
    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-50490"><a href="https://skillshub.online/study-at-the-open-university/">Study at The OU</a></li>
    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-51023"><a href="https://skillshub.online/ou-free-courses-on-openlearn/">OpenLearn</a></li>
    </ul><ul id="mobile_menu" class="et_mobile_menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-50379 current_page_item menu-item-50492 et_first_mobile_item"><a href="https://skillshub.online/" aria-current="page">OU and Uber Partnership</a></li>
    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-51792"><a href="https://skillshub.online/check-eligibility-launch/">Check your eligibility</a></li>
    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-50490"><a href="https://skillshub.online/study-at-the-open-university/">Study at The OU</a></li>
    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-51023"><a href="https://skillshub.online/ou-free-courses-on-openlearn/">OpenLearn</a></li>
    </ul></div>
                </div>				</div> <!-- #et-top-navigation -->
                </div> <!-- .container -->
                <div class="et_search_outer">
                    <div class="container et_search_form_container">
                        <form role="search" method="get" class="et-search-form" action="https://skillshub.online/">
                        <label class="da11y-screen-reader-text" for="et_pb_search_module_input_0">Search for...</label><label class="da11y-screen-reader-text" for="et_pb_search_module_input_0">Search for...</label><input type="search" class="et-search-field" placeholder="Search …" value="" name="s" title="Search for:" id="et_pb_search_module_input_0"><button type="submit" class="da11y-screen-reader-text">Search</button><button type="submit" class="da11y-screen-reader-text">Search</button>					</form>
                        <span class="et_close_search_field" aria-hidden="true"></span>
                    </div>
                </div>
            <div id="pbe-below-header-wa-wrap">
                </div></header> <!-- #main-header -->



    <div id="et-main-area">     
        <div id="main-content" tabindex="-1" role="main">
     
            <article id="post-50379" class="post-50379 page type-page status-publish hentry">
                <div class="entry-content">
                    <div id="et-boc" class="et-boc">
                        <div class="et-l et-l--post">
                            <div class="et_builder_inner_content et_pb_gutters3">
                                <div class="et_pb_section et_pb_section_0 et_pb_with_background et_section_regular">
                                    <div class="et_pb_row et_pb_row_0">
                                        <div class="et_pb_column et_pb_column_4_4 et_pb_column_0  et_pb_css_mix_blend_mode_passthrough et-last-child">           
                                            <div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_center">       
                                                <div class="et_pb_text_inner"><h1>The Open University and Uber</h1>
                                                    <h2>Driven by opportunity</h2>
                                                </div>
                                            </div> <!-- .et_pb_text -->
                                        </div> <!-- .et_pb_column -->                   
                                    </div> <!-- .et_pb_row -->                    
                                </div> <!-- .et_pb_section -->
                                <div class="et_pb_section et_pb_section_1 et_pb_with_background et_section_regular">
                                    <div class="et_pb_row et_pb_row_1">
                                         <div class="et_pb_column et_pb_column_4_4 et_pb_column_1  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                            <div class="et_pb_module et_pb_text et_pb_text_1 et_pb_bg_layout_light  et_pb_text_align_left">
                                                <div class="et_pb_text_inner">
                                                    <h2 style="text-align: center;">Uber and The Open University are partnering to help <strong>drivers</strong> or a <strong>family member</strong>&nbsp;reach their goals on and off the road.</h2>
                                                </div>
                                          </div> <!-- .et_pb_text -->
                                         </div> <!-- .et_pb_column -->                
                                    </div> <!-- .et_pb_row -->
                                </div> <!-- .et_pb_section -->
                                <div class="et_pb_section et_pb_section_2 et_pb_with_background et_section_regular">
                                    <div class="et_pb_row et_pb_row_2">
                                        <div class="et_pb_column et_pb_column_4_4 et_pb_column_2  et_pb_css_mix_blend_mode_passthrough et-last-child">                      
                                            <div class="et_pb_module et_pb_text et_pb_text_2 et_pb_bg_layout_light  et_pb_text_align_left">             <div class="et_pb_text_inner">
                                                    <h2 style="text-align: center;"><span>To register for the OU, please check your eligibility here. <strong>Please don’t register on the OU website</strong>.</span></h2>
                                                </div>
                                            </div> <!-- .et_pb_text -->
                                            <div class="et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_button_alignment_center et_pb_button_alignment_tablet_center et_pb_module ">
                                                <a class="et_pb_button et_pb_custom_button_icon et_pb_button_0 et_pb_bg_layout_light" href="https://skillshub.online/check-eligibility-launch/" data-icon="$">Check your eligibility</a>
                                            </div>
                                        </div> <!-- .et_pb_column -->                   
                                    </div> <!-- .et_pb_row -->
                                </div> <!-- .et_pb_section -->
                                
                            <!--MY SECTION I QAM LOOKING FOR-->
                            <?php 
                              if(have_rows('ou_flex_content')):
                                while (have_rows('ou_flex_content')): the_row(); 
                                if( get_row_layout() == 'standard'):?>
                                  <div class="et_pb_section et_pb_section_3 et_pb_with_background et_section_regular">
                                    <div class="et_pb_row et_pb_row_3">
                                        <div class="et_pb_column et_pb_column_1_2 et_pb_column_3  et_pb_css_mix_blend_mode_passthrough">
                                            <div class="et_pb_module et_pb_text et_pb_text_3 et_pb_bg_layout_light  et_pb_text_align_left">
                                                <div class="et_pb_text_inner">
                                                    <h2><span class="custom_h2"><?= the_sub_field('stext') ?></span></h2>
                                                    <p><?= the_sub_field('ssub_text') ?></p>
                                                </div>
                                            </div> <!-- .et_pb_text -->
                                        </div> <!-- .et_pb_column -->
                                        <div class="et_pb_column et_pb_column_1_2 et_pb_column_4  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                            <div class="et_pb_module et_pb_image et_pb_image_0">           
                                                <span class="et_pb_image_wrap "><img src="<?= the_sub_field('simage') ?>" alt=" " title="" sizes="(min-width: 0px) and (max-width: 480px) 480px, (min-width: 481px) 540px, 100vw"></span>
                                            </div>
                                        </div> <!-- .et_pb_column -->
                                    </div> <!-- .et_pb_row -->    
                                  </div> <!-- .et_pb_section -->
                                  <?php endif; ?>
                                  <?php if( get_row_layout() == 'extra'):?>
                                  <div class="et_pb_section et_pb_section_4 et_pb_with_background et_section_regular">                 
                                    <div class="et_pb_row et_pb_row_4">
                                        <div class="et_pb_column et_pb_column_1_2 et_pb_column_5  et_pb_css_mix_blend_mode_passthrough">
                                            <div class="et_pb_module et_pb_image et_pb_image_1">
                                              <?php if(get_sub_field('eimage')):?>
                                                <span class="et_pb_image_wrap "><img src="<?= the_sub_field('eimage') ?>" alt=" " title="" sizes="(min-width: 0px) and (max-width: 480px) 480px, (min-width: 481px) 540px, 100vw"></span>
                                              <?php endif; ?>
                                            </div>
                                        </div> <!-- .et_pb_column -->
                                        <div class="et_pb_column et_pb_column_1_2 et_pb_column_6  et_pb_css_mix_blend_mode_passthrough et-last-child">                  
                                            <div class="et_pb_module et_pb_text et_pb_text_4 et_pb_bg_layout_light  et_pb_text_align_left">
                                                <div class="et_pb_text_inner">
                                                    <h2><span class="custom_h2"><?= the_sub_field('etext') ?></span></h2>
                                                    <p>This partnership provides you or a family member the opportunity to access a high-quality education to help you get ahead in life.</p>
                                                    <p>Being able to work and learn flexibly is an essential part of unlocking economic opportunity. The Open University specialises in online flexible courses, allowing people balance their studies around their other commitments of work and family.</p>
                                                </div>
                                            </div> <!-- .et_pb_text -->
                                        </div> <!-- .et_pb_column -->
                                    </div> <!-- .et_pb_row -->
                                </div>
                                <?php endif; ?>
                                <?php if( get_row_layout() == 'showcase'):?>
                                  <div class="et_pb_section et_pb_section_4 et_pb_with_background et_section_regular">                 
                                    <div class="et_pb_row et_pb_row_4">
                                        <div class="et_pb_column et_pb_column_1_2 et_pb_column_5  et_pb_css_mix_blend_mode_passthrough">
                                            <div class="et_pb_module et_pb_image et_pb_image_1">
                                              <?php if(get_sub_field('showcase_image')):?>
                                                <span class="et_pb_image_wrap "><img src="<?= the_sub_field('showcase_image') ?>" alt=" " title="" sizes="(min-width: 0px) and (max-width: 480px) 480px, (min-width: 481px) 540px, 100vw"></span>
                                              <?php endif; ?>
                                            </div>
                                        </div> <!-- .et_pb_column -->
                                        <div class="et_pb_column et_pb_column_1_2 et_pb_column_6  et_pb_css_mix_blend_mode_passthrough et-last-child">                  
                                            <div class="et_pb_module et_pb_text et_pb_text_4 et_pb_bg_layout_light  et_pb_text_align_left">
                                                <div class="et_pb_text_inner">
                                                    <h2><span class="custom_h2"><?= the_sub_field('showcasetitle') ?></span></h2>
                                                    <p>This partnership provides you or a family member the opportunity to access a high-quality education to help you get ahead in life.</p>
                                                    <p>Being able to work and learn flexibly is an essential part of unlocking economic opportunity. The Open University specialises in online flexible courses, allowing people balance their studies around their other commitments of work and family.</p>
                                                </div>
                                            </div> <!-- .et_pb_text -->
                                        </div> <!-- .et_pb_column -->
                                    </div> <!-- .et_pb_row -->
                                </div>
                                <?php endif; ?>
                                <?php endwhile; endif; ?>
                          
                               
                            <!-- END MY SECTION I QAM LOOKING FOR-->

                                <!-- .et_pb_section -->
                                <div class="et_pb_section et_pb_section_10 et_pb_with_background et_section_regular"> 
                                    <!-- .et_pb_row -->
                                    <div class="et_pb_row et_pb_row_13">
                                        <div class="et_pb_column et_pb_column_4_4 et_pb_column_22  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                            <div class="et_pb_module et_pb_text et_pb_text_15 et_pb_bg_layout_light  et_pb_text_align_left">
                                                <div class="et_pb_text_inner"><h2>Your questions answered</h2></div>
                                            </div> <!-- .et_pb_text -->
                                            <div class="et_pb_module et_pb_accordion et_pb_accordion_1">
                                                <div class="et_pb_toggle et_pb_module et_pb_accordion_item et_pb_accordion_item_1  et_pb_toggle_close" tabindex="0">                                     
                                                    <h5 class="et_pb_toggle_title">Who is eligible for 100% tuition coverage?</h5>
                                                    <div class="et_pb_toggle_content clearfix">
                                                        Uber Partner-Drivers who have achieved Gold, Platinum or Diamond status through the Uber Pro program in the UK, and have completed 1,000 or more lifetime trips are eligible to receive full tuition coverage for online courses at The Open University. Qualifying drivers have the option to transfer this reward to an eligible family member, including a spouse or domestic partner, child or dependent, sibling, or parent or legal guardian.
                                                    </div> <!-- .et_pb_toggle_content -->
                                                </div> <!-- .et_pb_toggle -->
                                                <!-- .et_pb_toggle -->
                                                <div class="et_pb_toggle et_pb_module et_pb_accordion_item et_pb_accordion_item_1  et_pb_toggle_close" tabindex="0">                                     
                                                        <h5 class="et_pb_toggle_title">Who is eligible for 100% tuition coverage?</h5>
                                                        <div class="et_pb_toggle_content clearfix">
                                                            Uber Partner-Drivers who have achieved Gold, Platinum or Diamond status through the Uber Pro program in the UK, and have completed 1,000 or more lifetime trips are eligible to receive full tuition coverage for online courses at The Open University. Qualifying drivers have the option to transfer this reward to an eligible family member, including a spouse or domestic partner, child or dependent, sibling, or parent or legal guardian.
                                                        </div> <!-- .et_pb_toggle_content -->
                                                    </div> <!-- .et_pb_toggle -->
                                            </div> <!-- .et_pb_accordion -->
                                        </div> <!-- .et_pb_column -->
                                    </div> <!-- .et_pb_row -->   
                                </div> <!-- .et_pb_section -->
                                
                                <div class="et_pb_section et_pb_section_12 et_pb_with_background et_section_regular">                               
                                    <div class="et_pb_row et_pb_row_14 et_pb_gutters1">
                                        <div class="et_pb_column et_pb_column_1_3 et_pb_column_23  et_pb_css_mix_blend_mode_passthrough">
                                            <div class="et_pb_module et_pb_image et_pb_image_10">
                                               <span class="et_pb_image_wrap "><img src="https://skillshub.online/wp-content/uploads/2019/09/OU_Uber_NewLogo_footer.png" alt="The Open University and Uber partnership logo" title="" srcset="https://skillshub.online/wp-content/uploads/2019/09/OU_Uber_NewLogo_footer.png 322w, https://skillshub.online/wp-content/uploads/2019/09/OU_Uber_NewLogo_footer-300x89.png 300w" sizes="(max-width: 322px) 100vw, 322px"></span>
                                            </div>
                                        </div> <!-- .et_pb_column -->
                                        <div class="et_pb_column et_pb_column_1_3 et_pb_column_24  et_pb_css_mix_blend_mode_passthrough">                <div class="et_pb_with_border et_pb_module et_pb_sidebar_0 et_pb_widget_area et_pb_bg_layout_dark clearfix et_pb_widget_area_left et_pb_sidebar_no_border">
                                            </div> <!-- .et_pb_widget_area -->
                                        </div> <!-- .et_pb_column -->
                                        <div class="et_pb_column et_pb_column_1_3 et_pb_column_25  et_pb_css_mix_blend_mode_passthrough et-last-child">                                 
                                        
                                        </div> <!-- .et_pb_column -->  
                                    </div> <!-- .et_pb_row -->
                                    <div class="et_pb_row et_pb_row_15">
                                        <div class="et_pb_column et_pb_column_4_4 et_pb_column_26  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                       </div> <!-- .et_pb_column -->                                    
                                    </div> <!-- .et_pb_row -->
                                    <div class="et_pb_row et_pb_row_16">
                                        <div class="et_pb_column et_pb_column_1_2 et_pb_column_27  et_pb_css_mix_blend_mode_passthrough">
                                        </div> <!-- .et_pb_column -->
                                        <div class="et_pb_column et_pb_column_1_2 et_pb_column_28  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                        </div> <!-- .et_pb_column -->
                                    </div> <!-- .et_pb_row -->
                                    <div class="et_pb_row et_pb_row_17">
                                        <div class="et_pb_column et_pb_column_4_4 et_pb_column_29  et_pb_css_mix_blend_mode_passthrough et-last-child">
                                        </div> <!-- .et_pb_column --> 
                                    </div> <!-- .et_pb_row -->  
                                </div> <!-- .et_pb_section -->		
                            </div><!-- .et_builder_inner_content -->
                        </div><!-- .et-l -->
                    </div><!-- #et-boc -->
                </div> <!-- .entry-content -->
            </article> <!-- .et_pb_post -->                                          
        </div> <!-- #main-content -->         
    </div> <!-- #et-main-area -->
</div> <!-- #page-container -->
    
        </body></html>